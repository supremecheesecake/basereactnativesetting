import { Navigation } from "react-native-navigation";

// Import redux store
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore';
const store = configureStore();

// Import screens
import SideMenu from './src/screens/SideMenu/SideMenu';
import TopbarTitle from './src/screens/TopbarTitle/TopbarTitle';
import TopbarBackground from './src/screens/TopbarBackground/TopbarBackground';
import LoadingScreen from './src/screens/LoadingScreen/LoadingScreen';
import HomeScreen from './src/screens/HomeScreen/HomeScreen';
import ScreenWithSidebar from './src/screens/ScreenWithSidebar/ScreenWithSidebar';
import ScreenWithTabs from './src/screens/ScreenWithTabs/ScreenWithTabs';
import SecondTab from './src/screens/SecondTab/SecondTab';
import ScreenWithTabsAndSidebar from './src/screens/ScreenWithTabsAndSidebar/ScreenWithTabsAndSidebar';
import ScreenWithCustomTopbar from './src/screens/ScreenWithCustomTopbar/ScreenWithCustomTopbar';

import { setTopbarlessRootToScreen } from './src/utility/navigation';

Navigation.setDefaultOptions({
  animations: {
    setRoot: {
      enabled: 'true',
      alpha: {
        from: 0,
        to: 1,
        duration: 400,
        startDelay: 0,
        interpolation: 'accelerate'
      }
    }
  }
});

Navigation.registerComponentWithRedux(`weighttrack.LoadingScreen`, () => LoadingScreen, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.HomeScreen`, () => HomeScreen, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.ScreenWithSidebar`, () => ScreenWithSidebar, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.ScreenWithTabs`, () => ScreenWithTabs, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.SecondTab`, () => SecondTab, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.ScreenWithTabsAndSidebar`, () => ScreenWithTabsAndSidebar, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.ScreenWithCustomTopbar`, () => ScreenWithCustomTopbar, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.SideMenu`, () => SideMenu, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.TopbarTitle`, () => TopbarTitle, Provider, store);
Navigation.registerComponentWithRedux(`weighttrack.TopbarBackground`, () => TopbarBackground, Provider, store);

Navigation.events().registerAppLaunchedListener(() => setTopbarlessRootToScreen('LoadingScreen'));