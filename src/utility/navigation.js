import {Navigation} from 'react-native-navigation';

function rootToScreen(name, options){
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name,
              options
            }
          }
        ]
      }
    }
  });
}

export function setRootToScreen(screen, options) {
  rootToScreen(
    "weighttrack."+screen,
    options
  )
}

export function setTopbarlessRootToScreen(screen) {
  rootToScreen(
    "weighttrack."+screen,
    {
      topBar: {
        visible: false,
        drawBehind: true,
      }
    }
  )
}

export function toggleSideMenu(visible) {
  Navigation.mergeOptions('SideMenu', {
    sideMenu: { 
      left: { 
        visible
      } 
    } 
  });
}



export function goToScreenWithTabs(componentId) {
  Navigation.push(componentId, {
    bottomTabs: {
      children: [
        {
          component: {
            name: 'weighttrack.ScreenWithTabs',
            options: {
              bottomTab: {
                text: 'Tab 1',
                icon: require('../assets/logo.png')
              }
            }
          }
        },
        {
          component: {
            name: 'weighttrack.SecondTab',
            options: {
              bottomTab: {
                text: 'Tab 2',
                icon: require('../assets/logo.png')
              }
            }
          }
        }
      ]
    }
  });
}

export function goToScreenWithSidebar(componentId) {
  Navigation.push(componentId, {
    sideMenu: {
      left: {
        component: {
          name: 'weighttrack.SideMenu',
          id: 'SideMenu',
          passProps: {
            parentScreen: componentId
          },
          options: {
            sideMenu: {
              left: {
                width: 260
              }
            }
          }
        }
      },
      center: {
        stack: {
          children: [{
            component: {
              name: 'weighttrack.ScreenWithSidebar',
              options: {
                topBar: {
                  leftButtons: [
                    {
                      id: 'toggleSideMenuBtn',
                      icon: require('../assets/burger.png')
                    }
                  ]
                }
              }
            }
          }]
        }
      },
    }
  });
}

export function goToScreenWithTabsAndSidebar(componentId) {
  Navigation.push(componentId, {
    sideMenu: {
      left: {
        component: {
          name: 'weighttrack.SideMenu',
          id: 'SideMenu',
          passProps: {
            parentScreen: componentId
          },
          options: {
            sideMenu: {
              left: {
                width: 260
              }
            }
          }
        }
      },
      center: {
        bottomTabs: {
          children: [
            {
              component: {
                name: 'weighttrack.ScreenWithTabsAndSidebar',
                options: {
                  bottomTab: {
                    text: 'Tab 1',
                    icon: require('../assets/logo.png')
                  },
                  topBar: {
                    leftButtons: [
                      {
                        id: 'toggleSideMenuBtn',
                        icon: require('../assets/burger.png')
                      }
                    ]
                  }
                }
              }
            },
            {
              component: {
                name: 'weighttrack.SecondTab',
                options: {
                  bottomTab: {
                    text: 'Tab 2',
                    icon: require('../assets/logo.png')
                  },
                  topBar: {
                    leftButtons: [
                      {
                        id: 'toggleSideMenuBtn',
                        icon: require('../assets/burger.png')
                      }
                    ]
                  }
                }
              }
            }
          ]
        }
      },
    }
  });
}

export function goToScreenWithCustomTopbar(componentId) {
  Navigation.push(componentId, {
    component: {
      name: 'weighttrack.ScreenWithCustomTopbar',
      options: {
        topBar: {
          title: {
            component: {
              name: 'weighttrack.TopbarTitle',
              alignment: 'center'
            }
          },
          background: {
            color: '#00ff00',
            component: {
              name: 'weighttrack.TopbarBackground'
            }
          }
        }
      }
    }
  });
}