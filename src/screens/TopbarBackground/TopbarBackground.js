import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

class TopbarBackground extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Custom Topbar Background</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: '#ddd',
    flex: 1,
    width: '100%'
  },
});

export default TopbarBackground;