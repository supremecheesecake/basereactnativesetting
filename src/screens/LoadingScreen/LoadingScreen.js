import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

import { setRootToScreen } from '../../utility/navigation';

class LoadingScreen extends Component {
  componentDidMount = () => {
    this.goToNextScreenTimeout = setTimeout(() => {
      setRootToScreen('HomeScreen', {
        topBar: {
          title: {
            text: 'Home Screen'
          }
        }
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearTimeout(this.goToNextScreenTimeout);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    padding: 20
  }
});

export default LoadingScreen;