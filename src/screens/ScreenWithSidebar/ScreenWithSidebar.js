import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

import { toggleSideMenu } from '../../utility/navigation';

class ScreenWithSidebar extends Component {
  state = {
    sideMenuVisible: false
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    this.setState(prevState => {
      return {
        ...prevState,
        sideMenuVisible: !prevState.sideMenuVisible
      }
    }, () => {
      if(buttonId === 'toggleSideMenuBtn') toggleSideMenu(this.state.sideMenuVisible);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>ScreenWithSidebar</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
});

export default ScreenWithSidebar;