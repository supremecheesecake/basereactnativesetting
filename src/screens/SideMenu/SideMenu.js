import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableNativeFeedback } from 'react-native';
import { Navigation } from 'react-native-navigation';

class SideMenu extends Component {
  state = {
    marginTop: 0
  }

  buttonPressHandler = () => {
    Navigation.pop(this.props.parentScreen);
  }

  componentDidMount = async() => {
    const constants = await Navigation.constants();
    this.setState({marginTop: constants.topBarHeight})
  }

  render() {
    return (
      <View style={[styles.container, {marginTop: this.state.marginTop}]}>
        <Text style={styles.menuTitle}>SideMenu</Text>
        <TouchableNativeFeedback 
          onPress={this.buttonPressHandler}>
          <View style={[styles.menuItem, styles.menuItem__back]}>
            <Text style={styles.menuItemText}>Back</Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'flex-start',
  },
  menuTitle: {
    padding: 10,
    paddingBottom: 20,
    paddingTop: 20,
    backgroundColor: '#333',
    color: '#fff'
  },
  menuItem: {
    padding: 10,
  },
  menuItem__back: {

  }
});

export default SideMenu;