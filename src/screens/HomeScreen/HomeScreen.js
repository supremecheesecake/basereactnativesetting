import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, View, Image, TextInput } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';

import { setSetting } from '../../store/actions/index';

import { 
  goToScreenWithTabs, 
  goToScreenWithSidebar, 
  goToScreenWithTabsAndSidebar, 
  goToScreenWithCustomTopbar } from '../../utility/navigation';

import Button from '../../components/Button/Button';

import logo from '../../assets/logo.png';

class HomeScreen extends Component {
  state = {
    buttonDisabled: true,
    settingValue: null
  }

  buttonPressHandler = (name) => {
    switch (name) {
      case 'ScreenWithTabs':
        goToScreenWithTabs(this.props.componentId);
        break;
      case 'ScreenWithSidebar':
        goToScreenWithSidebar(this.props.componentId);
        break;
      case 'ScreenWithTabsAndSidebar':
        goToScreenWithTabsAndSidebar(this.props.componentId);
        break;
      case 'ScreenWithCustomTopbar':
        goToScreenWithCustomTopbar(this.props.componentId);
        break;
    }
  }

  buttonToggleHandler = () => {
    this.setState(prevState => {
      return {
        ...prevState,
        buttonDisabled: !prevState.buttonDisabled
      }
    })
  }

  changeSettingHandler = () => {
    this.props.onSettingUpdate(this.state.settingValue);
    this.setState({settingValue: null});
  }

  retrieveSettingHandler = () => {
    this.setState({settingValue: this.props.baseValue})
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.logoContainer}>
          <Image
            source={logo}
            style={styles.logo}
          />
        </View>
        <View style={styles.buttonsContainer}>
          <Text style={styles.headingBig}>Go to screen:</Text>
          <Button
            onPress={this.buttonToggleHandler}
            type="special"
          >Toggle buttons enabled/disabled</Button>
          <Button
            onPress={() => this.buttonPressHandler('ScreenWithTabs')} 
            disabled={this.state.buttonDisabled}
          >Screen with tabs</Button>
          <Button
            onPress={() => this.buttonPressHandler('ScreenWithSidebar')} 
            disabled={this.state.buttonDisabled}
          >Screen with sidebar</Button>
          <Button
            onPress={() => this.buttonPressHandler('ScreenWithTabsAndSidebar')} 
            disabled={this.state.buttonDisabled}
          >Screen with tabs and sidebar</Button>
          <Button
            onPress={() => this.buttonPressHandler('ScreenWithCustomTopbar')} 
            disabled={this.state.buttonDisabled}
          >Screen with custom topbar</Button>
        </View>
        <View style={styles.formContainer}>
          <TextInput  
            underlineColorAndroid="#ccc" 
            style={styles.textInput}
            placeholder="New setting value" 
            value={this.state.settingValue}
            onChangeText={(val) => this.setState({settingValue: val})}
            autoCapitalize='none'
            autoCorrect={false}
          />
          <Button
            onPress={() => this.changeSettingHandler()}
          >Save new setting</Button>
          <Button
            onPress={() => this.retrieveSettingHandler()}
          >Load saved setting</Button>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },

  logoContainer: {
    padding: 20,
    marginBottom: 20,
    height: 160,
    alignItems: 'center',
  },
  logo: {
    height: 120,
    width: 120,
  },

  buttonsContainer: {
    alignItems: 'center',
    flex: 1,
    marginBottom: 20
  },
  headingBig: {
    fontSize: 18,
    marginBottom: 10
  },

  btn: {
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderRadius: 4,
    padding: 20,
    paddingBottom: 10,
    paddingTop: 10,
    elevation: 1,
    marginBottom: 10
  },
  btn__special: {
    backgroundColor: '#0992ad',
  },
  btn__disabled: {
    opacity: 0.2
  },
  btnText: {
    textAlign: 'center',
    color: '#0992ad',
    fontSize: 12,
  },
  btnText__special: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 12,
  },

  formContainer: {
    alignItems: 'center',
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: '#eee'
  },
  textInput: {
    marginBottom: 10
  }
});

const mapStateToProps = state => {
  return {
    baseValue: state.settings.baseValue
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSettingUpdate: (newValue) => dispatch(setSetting(newValue))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);