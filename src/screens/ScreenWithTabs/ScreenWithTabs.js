import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

class ScreenWithTabs extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>ScreenWithTabs</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
});

export default ScreenWithTabs;