import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

class TopbarTitle extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Custom Topbar Title</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    flex: 1,
    width: '100%'
  },
});

export default TopbarTitle;