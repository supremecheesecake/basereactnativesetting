import React from 'react';
import { StyleSheet, Text, View, TouchableNativeFeedback } from 'react-native';

const Button = props => {
  const content = (
    <View 
      style={[
        styles.btn, 
        (props.disabled ? styles.btn__disabled : null), 
        (props.type ? styles[`btn__${props.type}`] : null)
      ]}
    >
      <Text style={[styles.btnText, (props.type ? styles[`btnText__${props.type}`] : null)]}>{props.children}</Text>
    </View>
  );
  if(props.disabled) return content;
  
  return(
    <TouchableNativeFeedback 
      {...props}>
      {content}
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  btn: {
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderRadius: 4,
    padding: 20,
    paddingBottom: 10,
    paddingTop: 10,
    elevation: 1,
    marginBottom: 10
  },
  btn__special: {
    backgroundColor: '#0992ad',
  },
  btn__disabled: {
    opacity: 0.2
  },
  btnText: {
    textAlign: 'center',
    color: '#0992ad',
    fontSize: 12,
  },
  btnText__special: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 12,
  }
});

export default Button;