import { GET_RECORDS, ADD_RECORD, DELETE_RECORD } from '../actions/actionTypes';

const initialState = {
  records: [],
  recordAdded: false
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_RECORDS:
      return {
        ...state
      }
    case ADD_RECORD:
      return {
        ...state,
        records: state.records.push(action.record),
        recordAdded: true
      }
    case DELETE_RECORD:
      return {
        ...state,
        records: state.records.filter((record) => {
          return record.id !== action.id
        })
      }
    default:
      return state;
  }
}

export default reducer;