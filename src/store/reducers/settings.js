import { SET_SETTING } from '../actions/actionTypes';

const initialState = {
  baseValue: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SETTING:
      return {
        ...state,
        baseValue: action.newValue
      };
    default:
      return state;
  }
}

export default reducer;