import { GET_RECORDS, ADD_RECORD, DELETE_RECORD } from './actionTypes';

export const getRecords = () => {
  return {
    type: GET_RECORDS
  }
}

export const addRecord = (record) => {
  return {
    type: ADD_RECORD,
    record
  }
}

export const deleteRecord = (id) => {
  return {
    type: DELETE_RECORD,
    id
  }
}