export const SET_SETTING = 'SET_SETTING';

export const GET_RECORDS = 'GET_RECORDS';
export const ADD_RECORD = 'ADD_RECORD';
export const DELETE_RECORD = 'DELETE_RECORD';