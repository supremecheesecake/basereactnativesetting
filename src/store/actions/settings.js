import { SET_SETTING, GET_SETTING } from './actionTypes';

export const setSetting = (newValue) => {
  return {
    type: SET_SETTING,
    newValue
  }
}