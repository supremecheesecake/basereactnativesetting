import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import recordsReducer from './reducers/records';
import settingsReducer from './reducers/settings';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  records: recordsReducer,
  settings: settingsReducer,
});

let composeEnhancers = compose;

if (__DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
  return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
}

export default configureStore;