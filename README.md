# Base React Native app starter for Android apps

**In short**: this is my base for potential react native project for Android. It's not much, as I could add here form validation, api connection etc. (but it shouldn't really be problem with Thunk). I wanted to focus on most basic functions that every app will use - navigation and redux.

# Includes
- React Native Navigation v2 with basic settings and few function in navigation.js utility file
- React Redux with Thunk - basic configuration (all screens registered with redux in mind)
- some structure and organisational choices based on [Maximilian Schwarzmüller's course](https://www.udemy.com/react-native-the-practical-guide/) - which I highly recomend as - in my opinion - he is the best and most active React instructor on Udemy. (Same goes for redux store configuration - also based on structure from this course).
- Loading Screen - as a place to check things like local storage, auth expiration on load etc. 

**Currently all above is configured for Android only. I didn't touch iOS config, as I have no Apple device to test it.**

# Additional info
It seems that as I'm writing these words (09.12.2018) React Native Navigation v2 plugin has some "issues". There are reported problems with some base functionalities not working the same way on both systems (Android & iOS), the documentation is incomplete and also it's not always up to date with newest React Native version. In other words, i'm not fully convinced if it's a wise choice for a long term app navigation - on the other hand I currently don't have time to delve into React Navigation plugin (which was strongly discouraged by many sources I've read, but that was mostly about v1, while currently it's at v3 stage).